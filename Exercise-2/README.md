10 LINUX COMMAND

What is a Linux command?
Linux is a family of open-source Unix-like operating systems based on the Linux kernel. The Linux command is a utility of the Linux operating system. All basic and advanced tasks can be done by executing commands. The commands are executed on the Linux terminal. The terminal is a command-line interface to interact with the system, which is similar to the command prompt in the Windows OS. Commands in Linux are case-sensitive.

1. bg

To send a process to the background, display stopped or background jobs.

![bg](images/bg.png)

2. du

To display total disk usage off the current directory.

![du](images/du-sh.png)

3. free

To display free and used memory ( -h for human readable, -m for MB, -g for GB.).

![free](images/free_-h.png)

4.  ip a

To display all network interfaces and IP address.
![ip](images/ip_a.png)

5. last

To display the last users who have logged onto the system.

![last](images/last.png)

6. lsof

To list all open files on the system.

![lsof](images/lsof.png)

7. netstat

The netstat command, meaning network statistics, is a Command Prompt command used to display very detailed information about how your computer is communicating with other computers or network devices.

![netstat](images/netstat.png)

8. uname

Use uname to show the information about the system your Linux distro is running.

![uname](images/uname.png)

9. w

To show who is logged in and what they are doing.

![w](images/w.png)

10. who

To show who is logged in only.

![who](images/who.png)

