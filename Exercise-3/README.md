The contents of /etc/passwd, /etc/group, /etc/sudoers

1. The content of /etc/passwd

![passwd](images/passwd.png)

2. The content of /etc/group

![group](images/groups-1.png)

/etc/group contd.
![group](images/groups-2.png)

3. The content of /etc/sudoers

![sudoers](images/sudoers.png)
